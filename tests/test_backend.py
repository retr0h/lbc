# vim: tabstop=4 shiftwidth=4 softtabstop=4

# Copyright 2012 John Dewey
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import unittest2 as unittest

from lbc import backend


class TestBackend(unittest.TestCase):
    def test_address_getter(self):
        b = backend.Backend('10.10.10.10')
        self.assertEqual('10.10.10.10', b.address)

    def test_port_getter(self):
        b = backend.Backend(None, port=80)
        self.assertEqual(80, b.port)

    def test_port_defaults(self):
        b = backend.Backend(None)
        self.assertEqual(80, b.port)

    def test_weight_getter(self):
        b = backend.Backend(None, weight=100)
        self.assertEqual(100, b.weight)

    def test_weight_defaults(self):
        b = backend.Backend(None)
        self.assertEqual(1, b.weight)
