# vim: tabstop=4 shiftwidth=4 softtabstop=4

# Copyright 2012 John Dewey
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import re
import sys

import unittest2 as unittest

from lbc import backend
from lbc import pool
from lbc import vip


class TestVIP(unittest.TestCase):
    def setUp(self):
        b1 = backend.Backend('1.1.1.1')
        b2 = backend.Backend('2.2.2.2')
        p = pool.HAProxy([b1,b2])
        self._vip = vip.HAProxy(p)

    def test_port_defaults_getter(self):
        v = vip.VIP(None)
        self.assertEqual(80, v.port)

    def test_haproxy_template_has_timeout(self):
        self._vip.create()
        output = sys.stdout.getvalue().strip()
        self.assertIn('timeout client 600000', output)

    def test_haproxy_template_has_predictor(self):
        self._vip.create()
        output = sys.stdout.getvalue().strip()
        self.assertIn('balance roundrobin', output)

    def test_haproxy_template_has_timeout(self):
        self._vip.create()
        output = sys.stdout.getvalue().strip()
        self.assertIn('timeout connect 8000', output)

    def test_haproxy_template_has_timeout_server(self):
        self._vip.create()
        output = sys.stdout.getvalue().strip()
        self.assertIn('timeout server 600000', output)

    def test_haproxy_template_has_retries(self):
        self._vip.create()
        output = sys.stdout.getvalue().strip()
        self.assertIn('retries 5', output)

    def test_haproxy_template_has_servers(self):
        self._vip.create()
        output = sys.stdout.getvalue().strip()
        self.assertIn('server backend1 1.1.1.1:80 maxconn 50 check inter 40000',
                      output)
        self.assertIn('server backend2 2.2.2.2:80 maxconn 50 check inter 40000',
                      output)
