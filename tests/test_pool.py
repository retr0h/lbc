# vim: tabstop=4 shiftwidth=4 softtabstop=4

# Copyright 2012 John Dewey
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import unittest2 as unittest

from lbc import backend
from lbc import pool


class TestPool(unittest.TestCase):
    def test_raises_not_implemented(self):
        with self.assertRaises(NotImplementedError):
            pool.Pool(None)

    def test_haproxy_predictor_defaults_getter(self):
        p = pool.HAProxy(None)
        self.assertEqual('roundrobin', p.predictor)

    def test_haproxy_backends_getter(self):
        b1 = backend.Backend(None)
        b2 = backend.Backend(None)
        l = [b1, b2]
        p = pool.HAProxy(l)
        self.assertEqual(l, p.backends)

    def test_haproxy_timeout_server_defaults_getter(self):
        p = pool.HAProxy(None, timeout_server=100)
        self.assertEqual(100, p.timeout_server)

    def test_haproxy_timeout_server_defaults(self):
        p = pool.HAProxy(None)
        self.assertEqual(600000, p.timeout_server)

    def test_haproxy_timeout_getter(self):
        p = pool.HAProxy(None, timeout=100)
        self.assertEqual(100, p.timeout)

    def test_haproxy_timeout_defaults(self):
        p = pool.HAProxy(None)
        self.assertEqual(8000, p.timeout)

    def test_haproxy_retries_getter(self):
        p = pool.HAProxy(None, retries=100)
        self.assertEqual(100, p.retries)

    def test_haproxy_raises_on_invalid_predictor(self):
        with self.assertRaises(RuntimeError) as m:
            pool.HAProxy(None, predictor='invalid')
        msg = str(m.exception)
        self.assertEqual('Invalid or unsupported predictor!', msg)
