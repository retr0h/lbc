.. lbc documentation master file, created by
   sphinx-quickstart on Wed Sep 26 19:28:16 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to lbc's documentation!
========================================

Contents:

.. toctree::
   :maxdepth: 2

   code


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

