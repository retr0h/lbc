# vim: tabstop=4 shiftwidth=4 softtabstop=4

# Copyright 2012 John Dewey
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

DEFAULT_PORT = 80
DEFAULT_WEIGHT = 1


class Backend(object):
    """
    A class representing a backend node.  A backend is a node(s) responsible
    for replying to the load balancer.
    """

    def __init__(self, address, **kwargs):
        """
        Construct a backend with supplied args.

        :param address: A string containing the backend's ipv4 address.
        :param kwargs['port']: An integer containing the backend's port in
                               which to connect.
        :param kwargs['weight']: An integer containg the backend's weight.
        """
        self._address = address
        self._port = kwargs.get('port', DEFAULT_PORT)
        self._weight = kwargs.get('weight', DEFAULT_WEIGHT)

    @property
    def address(self):
        return self._address

    @property
    def port(self):
        return self._port

    @property
    def weight(self):
        return self._weight
