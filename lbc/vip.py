# vim: tabstop=4 shiftwidth=4 softtabstop=4

# Copyright 2012 John Dewey
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

DEFAULT_PORT = 80

from jinja import FileSystemLoader
from jinja.environment import Environment


class VIP(object):
    """
    A class representing the type of vip.
    """

    def __init__(self, pool, **kwargs):
        """
        Construct a vip with supplied args.

        :param pool: A list containing a pool object.
        :param kwargs['port']: An integer containing the vip's port in
                               which to connect.
        """
        self._pool = pool
        self._port = kwargs.get('port', DEFAULT_PORT)

    @property
    def port(self):
        return self._port

class HAProxy(VIP):
    DEFAULT_TIMEOUT_CLIENT = 600000
    DEFAULT_MAXCONN = 4096

    def __init__(self, pool, **kwargs):
        """
        Vendor-dependent.  Construct a vip with supplied args.

        :param pool: A list containing a pool object.
        :param kwargs['timeout_client']: An integer containing the vip's
                                         maximum inactivity time on the
                                         client side.
        :param kwargs['maxconn']: An integer containing the vip's maximum
                                  number of concurrent connections.
        """
        super(HAProxy, self).__init__(pool, **kwargs)
        self._timeout_client = kwargs.get('timeout_client',
                                          self.DEFAULT_TIMEOUT_CLIENT)
        self._maxconn = kwargs.get('maxconn', self.DEFAULT_MAXCONN)

    def create(self):
        """
        Vendor-dependent. Returns a ...
        """
        env = Environment()
        env.loader = FileSystemLoader('templates')
        template = env.get_template('haproxy')
        print template.render(frontend_port=self._port,
                              timeout_client=self._timeout_client,
                              pool=self._pool)
