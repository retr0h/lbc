# vim: tabstop=4 shiftwidth=4 softtabstop=4

# Copyright 2012 John Dewey
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


class Pool(object):
    DEFAULT_PREDICTOR = None
    PREDICTORS_MAP = {}

    """
    A class representing a pool of backend node(s).
    """

    def __init__(self, backends, **kwargs):
        """
        Construct a pool with supplied args.

        :param backends: A list containing backend objects.
        :param kwargs['predictor']: A string containing the pools
                                    load-balancing algorithm.
        """
        self._backends = backends
        self._predictor = self._get_predictor(**kwargs)

    @property
    def backends(self):
        return self._backends

    @property
    def predictor(self):
        return self._predictor

    def _get_predictor(self):
        raise NotImplementedError

class HAProxy(Pool):
    DEFAULT_PREDICTOR = 'round_robin'
    PREDICTORS_MAP = {
        'round_robin': 'roundrobin'
    }

    DEFAULT_TIMEOUT_SERVER = 600000
    DEFAULT_TIMEOUT = 8000
    DEFAULT_RETRIES = 5

    def __init__(self, backends, **kwargs):
        """
        Vendor-dependent.  Construct a pool with supplied args.

        :param backends: A list containing backend objects.
        :param kwargs['timeout_server']: An integer containing the pool's
                                         maximum inactivity time on the
                                         server side.
        :param kwargs['timeout']: An integer containing the pool's time
                                  to wait for a connection attempt to
                                  a server to succeed.
        :param kwargs['retries']: An integer containing the pool's maximum
                                  number of retries to perform on a backend
                                  after a connection failure.
        """
        super(HAProxy, self).__init__(backends, **kwargs)
        self._timeout_server = kwargs.get('timeout_server',
                                          self.DEFAULT_TIMEOUT_SERVER)
        self._timeout = kwargs.get('timeout',
                                    self.DEFAULT_TIMEOUT)
        self._retries = kwargs.get('retries',
                                    self.DEFAULT_RETRIES)

    @property
    def timeout_server(self):
        return self._timeout_server

    @property
    def timeout(self):
        return self._timeout

    @property
    def retries(self):
        return self._retries

    def _get_predictor(self, **kwargs):
        """
        Vendor-dependent. Returns a string with the type of predictor.

        :param kwargs['predictor']: A string containing the pools
                                    load-balancing algorithm.
        :raises RuntimeError if predictor is unknown.
        """
        predictor = kwargs.get('predictor', self.DEFAULT_PREDICTOR)
        try:
            return self.PREDICTORS_MAP[predictor]
        except KeyError:
            msg = "Invalid or unsupported predictor!"
            raise RuntimeError(msg)
