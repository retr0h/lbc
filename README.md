# lbc

Configure various load balancers via a single specification.

## Supported Load Balancers

* [HAProxy](http://haproxy.1wt.eu) - Toying around with a configuration
  generator.

TODO:

* [F5](http://www.f5.com)
* [Nginx](http://wiki.nginx.org/Main)

## Usage

Create a basic HAProxy roundrobin configuration, with two backend servers.

    from lbc import backend
    from lbc import balancer
    from lbc import pool

    b1 = backend.backend('10.10.10.10')
    b2 = backend.backend('10.10.10.20')
    p = backend.pool([b1, b2], predictor='round_robin')
    vip = vip.HAProxy(p, port=80)
    vip.execute()

## Development

Setup virtualenv

    $ pip install virtualenv
    $ pip install virtualenvwrapper

Creating a virtualenv and installing dependencies

    $ mkvirtualenv lbc --no-site-packages
    $ workon lbc
    $ pip install -r requirements.txt

## Testing

    $ workon lbc
    $ nosetest
